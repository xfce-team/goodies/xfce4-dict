# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR The Xfce development team.
# This file is distributed under the same license as the xfce4-dict package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfce4-dict 0.8.6git-6d971cb\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/apps/xfce4-dict\n"
"POT-Creation-Date: 2024-09-04 11:23+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: panel-plugin/xfce4-dict-plugin.desktop.in:5 src/xfce4-dict.desktop.in:5
#: lib/gui.c:759 lib/prefs.c:266
msgid "Dictionary"
msgstr ""

#: panel-plugin/xfce4-dict-plugin.desktop.in:6
msgid "A plugin to query different dictionaries."
msgstr ""

#: panel-plugin/xfce4-dict-plugin.c:273 panel-plugin/xfce4-dict-plugin.c:401
#: src/xfce4-dict.c:194 lib/spell.c:242 lib/gui.c:538
msgid "Ready"
msgstr ""

#: panel-plugin/xfce4-dict-plugin.c:335
msgid "Look up a word"
msgstr ""

#: panel-plugin/xfce4-dict-plugin.c:374 lib/gui.c:790
msgid "Search term"
msgstr ""

#: src/xfce4-dict.c:49
msgid "Search the given text using a Dict server(RFC 2229)"
msgstr ""

#: src/xfce4-dict.c:50
msgid "Search the given text using a web-based search engine"
msgstr ""

#: src/xfce4-dict.c:51
msgid "Check the given text with a spell checker"
msgstr ""

#: src/xfce4-dict.c:52
msgid "Grab the focus on the text field in the panel"
msgstr ""

#: src/xfce4-dict.c:53
msgid "Start stand-alone application even if the panel plugin is loaded"
msgstr ""

#: src/xfce4-dict.c:54
msgid "Grabs the PRIMARY selection content and uses it as search text"
msgstr ""

#: src/xfce4-dict.c:55
msgid "Be verbose"
msgstr ""

#: src/xfce4-dict.c:56
msgid "Show version information"
msgstr ""

#: src/xfce4-dict.c:127
msgid "[TEXT]"
msgstr ""

#: src/xfce4-dict.c:143
#, c-format
msgid "Please report bugs to <%s>."
msgstr ""

#: src/xfce4-dict.desktop.in:6
msgid "Dictionary Client"
msgstr ""

#: src/xfce4-dict.desktop.in:7
msgid "A client program to query different dictionaries"
msgstr ""

#: lib/spell.c:74
msgid "Spell Checker Results:"
msgstr ""

#: lib/spell.c:100
#, c-format
msgid "%d suggestion found."
msgid_plural "%d suggestions found."
msgstr[0] ""
msgstr[1] ""

#: lib/spell.c:104
#, c-format
msgid "Suggestions for \"%s\" (%s):"
msgstr ""

#: lib/spell.c:121
#, c-format
msgid "\"%s\" is spelled correctly (%s)."
msgstr ""

#: lib/spell.c:133
#, c-format
msgid "No suggestions could be found for \"%s\" (%s)."
msgstr ""

#. translation hint:
#. * Error while executing <spell command, e.g. "aspell"> (<error message>)
#: lib/spell.c:163
#, c-format
msgid "Error while executing \"%s\" (%s)."
msgstr ""

#: lib/spell.c:201
msgid "Please set the spell check command in the preferences dialog."
msgstr ""

#: lib/spell.c:207
msgid "Invalid input"
msgstr ""

#: lib/spell.c:246
#, c-format
msgid "Process failed (%s)"
msgstr ""

#: lib/speedreader.c:74 lib/speedreader.c:696
msgid "P_ause"
msgstr ""

#: lib/speedreader.c:75
msgid "_Resume"
msgstr ""

#: lib/speedreader.c:249 lib/speedreader.c:698
msgid "S_top"
msgstr ""

#: lib/speedreader.c:257
msgid "Running"
msgstr ""

#: lib/speedreader.c:260
msgid "Finished"
msgstr ""

#: lib/speedreader.c:261
msgid "_Back"
msgstr ""

#: lib/speedreader.c:269
msgid "Speed Reader"
msgstr ""

#: lib/speedreader.c:370
msgid "You must enter a text."
msgstr ""

#: lib/speedreader.c:527
msgid "Choose a file to load"
msgstr ""

#: lib/speedreader.c:530
msgid "_Cancel"
msgstr ""

#: lib/speedreader.c:531
msgid "_Open"
msgstr ""

#: lib/speedreader.c:556
#, c-format
msgid "The file '%s' could not be loaded."
msgstr ""

#: lib/speedreader.c:582
#, c-format
msgid "(display %d word at a time)"
msgid_plural "(display %d words at a time)"
msgstr[0] ""
msgstr[1] ""

#: lib/speedreader.c:606
msgid ""
"This is an easy speed reading utility to help train you to read faster. It "
"does this by flashing words at a rapid rate on the screen."
msgstr ""

#: lib/speedreader.c:611
msgid "_Words per Minute:"
msgstr ""

#: lib/speedreader.c:618
msgid "_Mark Paragraphs"
msgstr ""

#: lib/speedreader.c:625
msgid "Word _Grouping:"
msgstr ""

#: lib/speedreader.c:642
msgid "_Font Size:"
msgstr ""

#: lib/speedreader.c:663
msgid ""
"Enter some text here you would like to read.\n"
"\n"
"Be relaxed and make yourself comfortable, then press Start to begin speed "
"reading."
msgstr ""

#: lib/speedreader.c:676
msgid "Load the contents of a file"
msgstr ""

#: lib/speedreader.c:681
msgid ""
"Clear the contents of the text field and paste the contents of the clipboard"
msgstr ""

#: lib/speedreader.c:685
msgid "Clear the contents of the text field"
msgstr ""

#: lib/speedreader.c:697
msgid "_Start"
msgstr ""

#: lib/speedreader.c:699 lib/dictd.c:766 lib/gui.c:707 lib/gui.c:816
#: lib/prefs.c:268
msgid "_Close"
msgstr ""

#: lib/common.c:185
msgid "The search URL is empty. Please check your preferences."
msgstr ""

#: lib/common.c:191
msgid "Browser could not be opened. Please check your preferences."
msgstr ""

#: lib/common.c:231
msgid "Invalid non-UTF8 input"
msgstr ""

#: lib/common.c:567
msgid "Error"
msgstr ""

#: lib/common.c:570
msgid "warning"
msgstr ""

#. for translators: the first wildcard is the search term, the second wildcard
#. * is the name of the preferred web search engine
#: lib/dictd.c:381
#, c-format
msgid "Search \"%s\" using \"%s\""
msgstr ""

#: lib/dictd.c:388
msgid "Web Search:"
msgstr ""

#: lib/dictd.c:411 lib/dictd.c:719 lib/dictd.c:728 lib/dictd.c:817
#: lib/dictd.c:826
msgid "Could not connect to server."
msgstr ""

#: lib/dictd.c:417
msgid "The server is not ready."
msgstr ""

#: lib/dictd.c:424
msgid "Invalid dictionary specified. Please check your preferences."
msgstr ""

#: lib/dictd.c:432 lib/dictd.c:472 lib/dictd.c:851
msgid "Unknown error while querying the server."
msgstr ""

#: lib/dictd.c:447
msgid "Dictionary Results:"
msgstr ""

#: lib/dictd.c:450
#, c-format
msgid "No matches could be found for \"%s\"."
msgstr ""

#: lib/dictd.c:477
#, c-format
msgid "%d definition found."
msgid_plural "%d definitions found."
msgstr[0] ""
msgstr[1] ""

#: lib/dictd.c:690
#, c-format
msgid "Querying %s..."
msgstr ""

#: lib/dictd.c:749
msgid "An error occurred while querying server information."
msgstr ""

#: lib/dictd.c:762
#, c-format
msgid "Server Information for \"%s\""
msgstr ""

#: lib/dictd.c:846
msgid "The server doesn't offer any databases."
msgstr ""

#: lib/gui.c:361
msgid "Copy Link"
msgstr ""

#: lib/gui.c:371
msgid "Search"
msgstr ""

#: lib/gui.c:589
msgid "F_ind"
msgstr ""

#. File Menu
#: lib/gui.c:671
msgid "_File"
msgstr ""

#: lib/gui.c:678 lib/gui.c:804
msgid "Speed _Reader"
msgstr ""

#: lib/gui.c:693
msgid "_Preferences"
msgstr ""

#: lib/gui.c:707 lib/gui.c:816
msgid "_Quit"
msgstr ""

#. Help Menu
#: lib/gui.c:717
msgid "_Help"
msgstr ""

#: lib/gui.c:724
msgid "About"
msgstr ""

#: lib/gui.c:828
msgid "Search with:"
msgstr ""

#: lib/gui.c:832
msgid "_Dictionary Server"
msgstr ""

#: lib/gui.c:838
msgid "_Web Service"
msgstr ""

#: lib/gui.c:846
msgid "_Spell Checker"
msgstr ""

#: lib/gui.c:971
msgid "A client program to query different dictionaries."
msgstr ""

#: lib/gui.c:975
msgid "translator-credits"
msgstr ""

#: lib/gui.c:978
msgid "Xfce4 Dictionary"
msgstr ""

#: lib/prefs.c:52
msgid "dict.leo.org - German <-> English"
msgstr ""

#: lib/prefs.c:53
msgid "dict.leo.org - German <-> French"
msgstr ""

#: lib/prefs.c:54
msgid "dict.leo.org - German <-> Spanish"
msgstr ""

#: lib/prefs.c:55
msgid "dict.leo.org - German <-> Italian"
msgstr ""

#: lib/prefs.c:56
msgid "dict.leo.org - German <-> Chinese"
msgstr ""

#: lib/prefs.c:57
msgid "dict.cc - Dictionary"
msgstr ""

#: lib/prefs.c:58
msgid "Dictionary.com"
msgstr ""

#: lib/prefs.c:59
msgid "TheFreeDictionary.com"
msgstr ""

#: lib/prefs.c:60
msgid "Wikipedia, the free encyclopedia (EN)"
msgstr ""

#: lib/prefs.c:61
msgid "Wiktionary, the free dictionary (EN)"
msgstr ""

#: lib/prefs.c:62
msgid "Merriam-Webster Online Dictionary"
msgstr ""

#: lib/prefs.c:63
msgid "Clear"
msgstr ""

#: lib/prefs.c:98
msgid "You have chosen an invalid dictionary."
msgstr ""

#: lib/prefs.c:299
msgid "General"
msgstr ""

#: lib/prefs.c:301
msgid "<b>Default search method:</b>"
msgstr ""

#: lib/prefs.c:306 lib/prefs.c:453
msgid "Dictionary Server"
msgstr ""

#: lib/prefs.c:314 lib/prefs.c:561
msgid "Web Service"
msgstr ""

#: lib/prefs.c:322 lib/prefs.c:608
msgid "Spell Checker"
msgstr ""

#: lib/prefs.c:330
msgid "Last used method"
msgstr ""

#: lib/prefs.c:338
msgid "<b>Colors:</b>"
msgstr ""

#: lib/prefs.c:344
msgid "Links:"
msgstr ""

#: lib/prefs.c:345
msgid "Phonetics:"
msgstr ""

#: lib/prefs.c:346
msgid "Spelled correctly:"
msgstr ""

#: lib/prefs.c:347
msgid "Spelled incorrectly:"
msgstr ""

#: lib/prefs.c:398
msgid "<b>Panel Text Field:</b>"
msgstr ""

#: lib/prefs.c:404
msgid "Show text field in the panel"
msgstr ""

#. panel entry size
#: lib/prefs.c:411
msgid "Text field size:"
msgstr ""

#. server address
#: lib/prefs.c:456
msgid "Server:"
msgstr ""

#. server port
#: lib/prefs.c:466
msgid "Server Port:"
msgstr ""

#. dictionary
#: lib/prefs.c:476 lib/prefs.c:634
msgid "Dictionary:"
msgstr ""

#: lib/prefs.c:479
msgid "* (use all)"
msgstr ""

#: lib/prefs.c:481
msgid "! (use all, stop after first match)"
msgstr ""

#: lib/prefs.c:563
msgid "<b>Web search URL:</b>"
msgstr ""

#: lib/prefs.c:568
msgid "URL:"
msgstr ""

#: lib/prefs.c:588
msgid ""
"Enter the URL of a web site which offers translation or dictionary services. "
"Use {word} as placeholder for the searched word."
msgstr ""

#: lib/prefs.c:610
msgid "Spell Check Program:"
msgstr ""

#: lib/prefs.c:627
msgid ""
"<i>The spell check program can be 'enchant', 'aspell', 'ispell' or any other "
"spell check program which is compatible to the ispell command.\n"
"The icon shows whether the entered command exists.</i>"
msgstr ""
